package ru.nlmk.study.jse56.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.nlmk.study.jse56.config.HibernateConfig;
import ru.nlmk.study.jse56.model.User;

public class UserRepository {

    private SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public User getByNickname(String nickname) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("from User where nickName = :nickname").setParameter("nickname", nickname);
        User result = (User) query.uniqueResult();

        tx.commit();
        session.close();

        return result;
    }

}
