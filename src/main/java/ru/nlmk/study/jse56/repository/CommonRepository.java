package ru.nlmk.study.jse56.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.nlmk.study.jse56.config.HibernateConfig;
import ru.nlmk.study.jse56.model.User;

import java.io.Serializable;
import java.util.List;

public class CommonRepository {

    private SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public <T> void save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
    }



    /*// JPQL
    public List<Car> getAllCars() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<Car> cars = session.createQuery("SELECT c from Car c", Car.class).getResultList();

        tx.commit();
        session.close();

        return cars;
    }*/

}
