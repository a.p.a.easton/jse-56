package ru.nlmk.study.jse56.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.nlmk.study.jse56.config.HibernateConfig;
import ru.nlmk.study.jse56.model.Chat;

public class ChatRepository {

    private SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public Chat getByChatName(String chatName) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("from Chat where name = :chatName").setParameter("chatName", chatName);
        Chat result = (Chat) query.uniqueResult();

        tx.commit();
        session.close();

        return result;
    }

}
