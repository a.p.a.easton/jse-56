package ru.nlmk.study.jse56;

import ru.nlmk.study.jse56.model.Chat;
import ru.nlmk.study.jse56.model.Message;
import ru.nlmk.study.jse56.model.User;
import ru.nlmk.study.jse56.repository.ChatRepository;
import ru.nlmk.study.jse56.repository.CommonRepository;
import ru.nlmk.study.jse56.repository.UserRepository;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        CommonRepository repository = new CommonRepository();
        UserRepository userRepository = new UserRepository();
        ChatRepository chatRepository = new ChatRepository();

        System.out.println("Enter your nickname: ");
        String nickname = scanner.next();
        User user = userRepository.getByNickname(nickname);
        if(user == null){
            user = new User();
            user.setNickName(nickname);
            repository.save(user);
        }
        System.out.println("Which chat do you want to join?");
        String chatName = scanner.next();
        Chat chat = chatRepository.getByChatName(chatName);
        if(chat == null){
            chat = new Chat();
            chat.setName(chatName);
            repository.save(chat);
        }
        System.out.println("Write a message to the chat.");
        String chatMessage = null;
        while(chatMessage != "out") {
            chatMessage = scanner.next();
            Message message = new Message();
            message.setMessage(chatMessage);
            message.setUser(user);
            message.setChat(chat);
            repository.save(message);
        }
    }
}
